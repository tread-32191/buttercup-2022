package frc.robot;

import java.util.Arrays;
import edu.wpi.first.wpilibj.XboxController;
import frc.robot.Constants.DriveTrainConstants;
import frc.robot.Constants.OIConstants;
import frc.robot.commands.DriveTrain.*;
import frc.robot.commands.Pneumatics.*;
import frc.robot.subsystems.*;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

public class RobotContainer {
  private final Pneumatics m_pneumatics = new Pneumatics();
  private final DriveTrain m_driveTrain = new DriveTrain();

  enum driveType {
    Arcade,
    Tank,
    Curve
  }

  public XboxController controller = new XboxController(OIConstants.kControllerPort);

  public RobotContainer() {
    String driveTypeConstant = DriveTrainConstants.kDriveType;
    if (!Arrays.stream(DriveTrainConstants.kDriveOptions).anyMatch(DriveTrainConstants.kDriveType::equals)) {
      driveTypeConstant = "Arcade";
    }
    driveType dType = driveType.valueOf(driveTypeConstant);

    switch (dType) {
      case Arcade:
        // default:
        m_driveTrain.setDefaultCommand(new ArcadeDrive(m_driveTrain));
        break;

      case Tank:
        m_driveTrain.setDefaultCommand(new TankDrive(m_driveTrain));
        break;

      case Curve:
        m_driveTrain.setDefaultCommand(new CurvatureDrive(m_driveTrain));
        break;
    }
    configureButtonBindings();
  }

  private void configureButtonBindings() {
    new JoystickButton(controller, XboxController.Button.kX.value)
        .whenPressed(new FireCannon(m_pneumatics, "left"));

    new JoystickButton(controller, XboxController.Button.kB.value)
        .whenPressed(new FireCannon(m_pneumatics, "right"));

    new JoystickButton(controller, XboxController.Button.kY.value)
        .whileHeld(new Horn(m_pneumatics));

    new JoystickButton(controller, XboxController.Button.kBack.value)
        .whenPressed(new CompressorToggle(m_pneumatics));

    new JoystickButton(controller, XboxController.Button.kStart.value)
        .whenPressed(new DriveShift(m_pneumatics));
  }

}
