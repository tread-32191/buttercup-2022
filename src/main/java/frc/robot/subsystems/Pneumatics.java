package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.PneumaticsConstants;

public class Pneumatics extends SubsystemBase {
    private Solenoid m_driveShift, m_hornSolenoid, m_leftCannon, m_rightCannon, m_activeCannon;
    private Compressor m_compressor = new Compressor(PneumaticsModuleType.CTREPCM);

    public Pneumatics() {
        m_driveShift = new Solenoid(PneumaticsModuleType.CTREPCM, PneumaticsConstants.kDriveShift);
        m_hornSolenoid = new Solenoid(PneumaticsModuleType.CTREPCM, PneumaticsConstants.kHornSolenoid);
        m_leftCannon = new Solenoid(PneumaticsModuleType.CTREPCM, PneumaticsConstants.kLeftCannon);
        m_rightCannon = new Solenoid(PneumaticsModuleType.CTREPCM, PneumaticsConstants.kRightCannon);

        addChild("Gear Speed Solenoid", m_driveShift);
        addChild("Horn Solenoid", m_hornSolenoid);
        addChild("Left Cannon Solenoid", m_leftCannon);
        addChild("Right Cannon Solenoid", m_rightCannon);

    }

    public void setHorn(boolean hornState) {
        m_hornSolenoid.set(hornState);
        SmartDashboard.putBoolean("Horn", hornState);
    }

    public void operateCannon(String side, Boolean isFiring) {
        m_activeCannon = (side == "left") ? m_leftCannon : m_rightCannon;
        m_activeCannon.set(isFiring);
        if (isFiring) {
            SmartDashboard.putString("Cannon", side);
        } else {
            SmartDashboard.putString("Cannon", "none");
        }
    }

    public void setCompressor(boolean compressorState) {
        SmartDashboard.putBoolean("Compressor", compressorState);
        if (compressorState) {
            m_compressor.enableDigital();
        } else {
            m_compressor.disable();
        }
    }

    public void setGearing(String gearState) {
        SmartDashboard.putString("Gearing", gearState);
        if (gearState == "High") {
            m_driveShift.set(true);
        } else {
            m_driveShift.set(false);

        }
    }

}
