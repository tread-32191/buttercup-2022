package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DriveTrainConstants;
import frc.robot.Constants.OIConstants;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.motorcontrol.*;

@SuppressWarnings("unused")

public class DriveTrain extends SubsystemBase {

    private DifferentialDrive m_differentialDrive;

    public DriveTrain() {
        MotorController lLead = new VictorSP(DriveTrainConstants.kLeftLead);
        MotorController lFollow = new VictorSP(DriveTrainConstants.kLeftFollow);
        MotorControllerGroup lDrive = new MotorControllerGroup(lLead, lFollow);

        MotorController rLead = new VictorSP(DriveTrainConstants.kRightLead);
        MotorController rFollow = new VictorSP(DriveTrainConstants.kRightFollow);
        MotorControllerGroup rDrive = new MotorControllerGroup(rLead, rFollow);

        if (DriveTrainConstants.kInvertedSide == "Left") {
            lDrive.setInverted(true);
        } else {
            rDrive.setInverted(true);
        }

        m_differentialDrive = new DifferentialDrive(lDrive, rDrive);

        addChild("Differential Drive", m_differentialDrive);

        m_differentialDrive.setSafetyEnabled(true);
        m_differentialDrive.setDeadband(OIConstants.kJoystickDeadband);
    }

    public void tankDrive(double left, double right) {
        m_differentialDrive.tankDrive(left, right);
    }

    public void arcadeDrive(double speed, double turn) {
        m_differentialDrive.arcadeDrive(speed, turn);
    }

    public void curvatureDrive(double speed, double turn) {
        m_differentialDrive.curvatureDrive(speed, turn, true);
    }
}
