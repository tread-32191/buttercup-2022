package frc.robot;

public final class Constants {

    public static final class DriveTrainConstants {
        public static final String kInvertedSide = "Left";
        public static final double kSpeed = 0.75;
        public static final double kTurnRate = 0.85;
        public static final String[] kDriveOptions = { "Arcade", "Tank", "Curve" };
        public static final String kDriveType = "Arcade";

        public static final int kLeftLead = 1;
        public static final int kLeftFollow = 3;
        public static final int kRightLead = 0;
        public static final int kRightFollow = 2;
    }

    public static final class PneumaticsConstants {
        public static final double kFiringTime = 0.2;

        public static final int kLeftCannon = 0;
        public static final int kRightCannon = 1;
        public static final int kHornSolenoid = 2;
        public static final int kDriveShift = 3;
    }

    public static final class OIConstants {
        public static final double kJoystickDeadband = 0.1;
        public static final double kTriggerDeadband = 0.1;

        public static final int kControllerPort = 0;
        public static final int kControllerLYPort = 1;
        public static final int kControllerRXPort = 4;
        public static final int kControllerRYPort = 5;
    }

}
