package frc.robot.commands.Pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Pneumatics;
import edu.wpi.first.wpilibj.Timer;
import frc.robot.Constants.PneumaticsConstants;

public class FireCannon extends CommandBase {
    private final Pneumatics m_pneumatics;
    public String side;
    protected Timer m_timer = new Timer();

    public FireCannon(Pneumatics subsystem, String side) {
        m_pneumatics = subsystem;
        this.side = side;
        addRequirements(m_pneumatics);
    }

    @Override
    public void initialize() {
        m_pneumatics.operateCannon(side, true);
        m_timer.start();
    }

    @Override
    public void end(boolean inturrupted) {
        m_pneumatics.operateCannon(side, false);
        m_timer.stop();
        m_timer.reset();
    }

    @Override
    public boolean isFinished() {
        return m_timer.hasElapsed(PneumaticsConstants.kFiringTime);
    }
}
