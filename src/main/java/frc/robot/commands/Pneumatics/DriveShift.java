package frc.robot.commands.Pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Pneumatics;

public class DriveShift extends CommandBase {
    private final Pneumatics m_pneumatics;
    private String state;

    public DriveShift(Pneumatics subsystem) {
        m_pneumatics = subsystem;
    }

    @Override
    public void initialize() {
        state = (state == "High") ? "Low" : "High";
        m_pneumatics.setGearing(state);
    }

    @Override
    public void end(boolean inturrupted) {
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
