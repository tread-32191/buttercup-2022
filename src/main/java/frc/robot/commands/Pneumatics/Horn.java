package frc.robot.commands.Pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Pneumatics;

public class Horn extends CommandBase {
    private final Pneumatics m_pneumatics;

    public Horn(Pneumatics subsystem) {
        m_pneumatics = subsystem;
        addRequirements(m_pneumatics);
    }

    @Override
    public void initialize() {
        m_pneumatics.setHorn(true);
    }

    @Override
    public void end(boolean inturrupted) {
        m_pneumatics.setHorn(false);
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
