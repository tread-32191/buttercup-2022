package frc.robot.commands.Pneumatics;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Pneumatics;

public class CompressorToggle extends CommandBase {
    private final Pneumatics m_pneumatics;
    private boolean compressorState = true;

    public CompressorToggle(Pneumatics subsystem) {
        m_pneumatics = subsystem;
    }

    @Override
    public void initialize() {
        compressorState = !compressorState;
        m_pneumatics.setCompressor(compressorState);
    }

    @Override
    public void end(boolean inturrupted) {
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
