package frc.robot.commands.DriveTrain;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants.DriveTrainConstants;
import frc.robot.Constants.OIConstants;
import frc.robot.subsystems.DriveTrain;

public class ArcadeDrive extends CommandBase {
    private final DriveTrain m_driveTrain;

    public double speed, turn, speedController;

    private XboxController controller = new XboxController(OIConstants.kControllerPort);

    public ArcadeDrive(DriveTrain subsystem) {
        m_driveTrain = subsystem;
        addRequirements(m_driveTrain);
    }

    @Override
    public void initialize() {
    }

    @Override
    public void execute() {
        speedController = (controller.getRightTriggerAxis() > OIConstants.kTriggerDeadband)
                ? 1
                : DriveTrainConstants.kSpeed;

        speed = -controller.getRawAxis(OIConstants.kControllerLYPort);
        turn = controller.getRawAxis(OIConstants.kControllerRXPort);

        m_driveTrain.arcadeDrive(
                speed * speedController,
                turn * speedController * DriveTrainConstants.kTurnRate);
    }

    @Override
    public void end(boolean inturrupted) {
        m_driveTrain.tankDrive(0.0, 0.0);
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
